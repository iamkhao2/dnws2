using System;
using System.Text;

namespace DNWS
{
    public class Utilize
    {
        private string context;

        public Utilize()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<html><body>");
            context = sb.ToString();

        }
        public void PooledFunc(object state)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(String.Format("Processing request '{0}'", (string)state));
            sb.Append("<br>");
            Console.WriteLine("Processing request '{0}'", (string)state);
            //Thread.Sleep(2000);
            Console.WriteLine("Request processed");
            context += sb.ToString();
        }
        public override String ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("</body></html>");
            context += sb.ToString();
            return context;
        }
    }
}